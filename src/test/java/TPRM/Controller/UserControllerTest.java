package TPRM.Controller;

import TPRM.Controller.Rest.UserRestController;
import TPRM.Main;
import TPRM.Model.Role;
import TPRM.Model.User;
import TPRM.Repository.UserRepository;
import TPRM.Service.UserService;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Main.class)
//@AutoConfigureMockMvc
public class UserControllerTest {

    @InjectMocks
    private UserService userService = new UserService();

    @Mock
    private UserRestController userRestController;

    @Mock
    private UserRepository userRepository;

    @Test
    public void createAdminAccount() throws Exception {
        Set<Role> roleSet = new HashSet<>();
        Role role = new Role();
        role.setRoleName("ADMIN");
        roleSet.add(role);
        User user = new User("admin", "admin", roleSet);
        user.setId(1L);
        when(userService.findByUsername("admin")).thenReturn(Optional.of(user));
        assertThat(userService.loadUserByUsername("admin").getUsername(), Matchers.is("admin"));
    }

    @Test
    public void createGeneralUserAccount() throws Exception {
        Set<Role> roleSet = new HashSet<>();
        Role role = new Role();
        role.setRoleName("USER");
        roleSet.add(role);
        User user = new User("Test User", "password", roleSet);
        user.setId(1L);
        when(userService.findByUsername("Test User")).thenReturn(Optional.of(user));
        Optional<User> optionalUser = userService.findByUsername("Test User");
        assertThat(optionalUser.get().getRoles(), hasItem(Matchers.<Role>hasProperty("roleName", is("USER"))));
    }

}
