package TPRM.Controller;

import TPRM.Controller.Rest.AuthenticationRequest;
import TPRM.Controller.Rest.UserRestController;
import TPRM.Main;
import com.fasterxml.jackson.databind.ObjectMapper;
import groovy.util.logging.Slf4j;

import org.apache.http.HttpStatus;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.http.MediaType;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static io.restassured.RestAssured.given;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.springframework.security.web.FilterChainProxy;
@RunWith(SpringRunner.class)
//@WebAppConfiguration
@SpringBootTest(webEnvironment = DEFINED_PORT)
@ContextConfiguration
public class UserRestControllerTest {
    @LocalServerPort
    private int port;


    @Autowired
    ObjectMapper objectMapper;

    private String adminToken;
    private String userToken;
//    @Autowired
//    private FilterChainProxy springSecurityFilterChain;

    private MockMvc mvc;

    @Before
    public void setup() {
        RestAssured.port = 8083;
        adminToken = given()
                .contentType(ContentType.JSON)
                .body(AuthenticationRequest.builder().username("admin").password("admin").build())
                .when().post("/api/auth/login")
                .andReturn().jsonPath().getString("token");
        System.out.println(("Got token:" + adminToken));
        userToken = given()
                .contentType(ContentType.JSON)
                .body(AuthenticationRequest.builder().username("user").password("user").build())
                .when().post("/api/auth/login")
                .andReturn().jsonPath().getString("token");
        System.out.println(("Got token:" + userToken));
    }

    //@WithMockUser(username = "ADMIN", password = "ADMIN", roles = "ADMIN")
    @Test
    public void requestingUserInformationShouldReturnStatus200AsAdmin() throws Exception {
        given()
                .accept(ContentType.JSON)
                .header("Authorization", "Bearer "+adminToken)
                .when()
                .get("/api/user/all")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void requestingUserInformationShouldReturnStatus403AsStandardUser() throws Exception {
        given()
                .accept(ContentType.JSON)
                .header("Authorization", "Bearer "+userToken)
                .when()
                .get("/api/user/all")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_FORBIDDEN);
    }

    @Test
    public void requestingUserInformationShouldReturnStatus401AsAnonymous() throws Exception {
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/api/user/all")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }


    @Test
    public void requestUser1InfoAsUser() throws Exception {
        given()
                .accept(ContentType.JSON)
                .when()
                .header("Authorization", "Bearer "+userToken)
                .get("/api/user/1")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }


}
