const addAnswerRow = (id, event = 'change') => {
    document.getElementById(id).addEventListener(event, function() {
        console.log("test from " + id)
        let selection = document.getElementById(id).value;

        console.log(selection)
        checkOptionSelection(id,selection)
    });
}

function checkOptionSelection(id, selection){
    if(selection === "MULTI-CHOICE"){
        if(checkIfQuestionHasAnswerRow(id)){
            appendAnswerDiv(id)
        }
    }
}

var checkIfQuestionHasAnswerRow = function(id){
    let parent = $(document.getElementById(id)).parent();
    let childList = parent.find("div[class*='answers']")
    console.log(childList.length)
    return childList.length === 0;
}
const addDeleteEvent = (id, event = 'click') => {
    document.getElementById(id).addEventListener(event, function() {
        console.log("delete listener " + id)
        $(document.getElementById(id)).remove()
    });

}

function appendAnswerDiv(id){
    let parent = $(document.getElementById(id)).parent();
    let buttonId = parent.attr("id") + "answerButton"
    var html = '<div class="' + id + '.answers">' +
        '<input type="text">' +
        '<button type="button" id="'+ buttonId +'">Add Response</button>' +
        'SOME PLACEHOLDER TEXT' +
        '' +
        '</div>'

    parent.append(html)
}

$("button[class='answerButton']").on('click', function () {
    let button = $(this).get()
    console.log(button)
    let parent = $(this).parent()
    let list = parent.find("ol")
    let question = parent.find("input[type='hidden']")
    let fullString = question.attr("name")
    let cleanString = fullString.substring(0, fullString.length - 3);
    var value = parent.find("input[class='answerInput']").val()
    var count = list.children().length;
    console.log(count)
    console.log(cleanString)

    var html = '<li id="'+ cleanString + '.answers[' +  count + '].answer.item' + '">' + value +
        '<input  id="'+ cleanString + '.answers[' +  count + '].answer'+ '" type="hidden" name="'+ cleanString + '.answers[' +  count + '].answer' + '" value="' + value + '"/>' +
        '</li>'
    list.append(html)
    addDeleteEvent(cleanString + '.answers[' +  count + '].answer.item')

});

$("li").on('click', function () {
    $(this).remove()
})