$("input:checkbox").change(function(){
    console.log($(this).attr("checked"))
    var questionContainer = $(this).parent().parent();
    console.log(questionContainer.find("input[id*='parentQuestion']"))
    var parentId = questionContainer.find("input[id*='parentQuestion']").val();
    if($(this).attr("checked") === "checked"){
        console.log("test1")
        $(this).removeAttr("checked")
        if(parentId !== undefined){
            console.log("enable questions.")
            findQuestionsToDisable(parentId, false)
        }
    }else{
        console.log("test2")
        $(this).attr("checked", "checked")
        if(parentId !== undefined){
            console.log("disable questions.")
            findQuestionsToDisable(parentId, true)
        }
    }

    console.log($(this).get());
});


var findQuestionsToDisable = function(parentQuestionId, boolean){
    var dependentQuestionList = $( "input[id*='dependentQuestion']" );
    console.log(dependentQuestionList);
    dependentQuestionList.each(function(idx, li) {
        let item = $(li);
        if(item.val() === parentQuestionId){
            item.parent().find("input[id*='.answer']").prop('disabled', boolean)
            console.log(item.parent().find("input[id*='.answer']"))
        }
    });
}