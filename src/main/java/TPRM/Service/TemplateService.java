package TPRM.Service;

import TPRM.Model.SurveyTemplate;
import TPRM.Repository.SurveyTemplateRepository;
import TPRM.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TemplateService {
    @Autowired
    private SurveyTemplateRepository surveyTemplateRepository;

    public List<SurveyTemplate> findAllTemplates(){
        return (List<SurveyTemplate>) surveyTemplateRepository.findAll();
    }
    public void save(SurveyTemplate surveyTemplate){
        surveyTemplateRepository.save(surveyTemplate);
    }
    public Optional<SurveyTemplate> findById(Long id){
        return surveyTemplateRepository.findById(id);
    }
}
