package TPRM.Service;

import TPRM.Model.Question.QuestionBlock;
import TPRM.Repository.QuestionBlockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BlockService {
    @Autowired
    private QuestionBlockRepository questionBlockRepository;
    public List<QuestionBlock> findAllBlocks(){
        return (List<QuestionBlock>) questionBlockRepository.findAll();
    }
    public Optional<QuestionBlock> findById(Long id){
        return questionBlockRepository.findById(id);
    }
    public void save(QuestionBlock questionBlock){
        questionBlockRepository.save(questionBlock);
    }
}
