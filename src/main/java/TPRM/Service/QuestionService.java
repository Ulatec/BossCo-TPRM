package TPRM.Service;

import TPRM.Model.Question.Question;
import TPRM.Repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionService {
    @Autowired
    private QuestionRepository questionRepository;
    public List<Question> findAllBlocks(){
        return (List<Question>) questionRepository.findAll();
    }
    public Optional<Question> findById(Long id){
        return questionRepository.findById(id);
    }
    public void save(Question question){
        questionRepository.save(question);
    }
    public void delete(Question question){
        questionRepository.delete(question);
    }
}
