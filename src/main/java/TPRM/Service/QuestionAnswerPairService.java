package TPRM.Service;

import TPRM.Model.Question.QuestionAnswerPair;
import TPRM.Repository.QuestionAnswerPairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionAnswerPairService {
    @Autowired
    private QuestionAnswerPairRepository questionAnswerPairRepository;
    public List<QuestionAnswerPair> findAllBlocks(){
        return (List<QuestionAnswerPair>) questionAnswerPairRepository.findAll();
    }
    public Optional<QuestionAnswerPair> findById(Long id){
        return questionAnswerPairRepository.findById(id);
    }
    public void save(QuestionAnswerPair question){
        questionAnswerPairRepository.save(question);
    }
    public void delete(QuestionAnswerPair question){
        questionAnswerPairRepository.delete(question);
    }
}