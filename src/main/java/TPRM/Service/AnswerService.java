package TPRM.Service;

import TPRM.Model.Answer;
import TPRM.Repository.AnswerRepository;
import TPRM.Repository.QuestionBlockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerService {
    @Autowired
    private AnswerRepository answerRepository;

    public List<Answer> findAll(){
        return (List<Answer>) answerRepository.findAll();
    }
    public Optional<Answer> findById(Long id){
        return answerRepository.findById(id);
    }
    public void save(Answer answer){
        answerRepository.save(answer);
    }
    public void delete(Answer answer){
        answerRepository.delete(answer);
    }
}
