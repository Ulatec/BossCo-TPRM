package TPRM.Service;

import TPRM.Model.Question.QuestionBlockTemplate;
import TPRM.Repository.QuestionBlockTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BlockTemplateService {
    @Autowired
    private QuestionBlockTemplateRepository questionBlockRepository;
    public List<QuestionBlockTemplate> findAllBlocks(){
        return (List<QuestionBlockTemplate>) questionBlockRepository.findAll();
    }
    public Optional<QuestionBlockTemplate> findById(Long id){
        return questionBlockRepository.findById(id);
    }
    public void save(QuestionBlockTemplate questionBlock){
        questionBlockRepository.save(questionBlock);
    }
    public Optional<QuestionBlockTemplate> findByName(String name){
        return questionBlockRepository.findByTitle(name);
    }
    public boolean questionBlockTitleIsBlank(QuestionBlockTemplate questionBlockTemplate){
        return questionBlockTemplate.getTitle().replace(" ", "").equals("");
    }
}
