package TPRM.Service;

import TPRM.Model.TemplateModels.ScoringTemplate;
import TPRM.Repository.ScoringTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ScoringTemplateService {
    @Autowired
    private ScoringTemplateRepository scoringTemplateRepository;
    public List<ScoringTemplate> findAllTemplates(){
        return (List<ScoringTemplate>) scoringTemplateRepository.findAll();
    }
    public Optional<ScoringTemplate> findById(Long id){
        return scoringTemplateRepository.findById(id);
    }
    public void save(ScoringTemplate scoringTemplate){
        scoringTemplateRepository.save(scoringTemplate);
    }
    public void delete(ScoringTemplate scoringTemplate){
        scoringTemplateRepository.delete(scoringTemplate);
    }
}
