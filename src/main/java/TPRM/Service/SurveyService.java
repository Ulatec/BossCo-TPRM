package TPRM.Service;

import TPRM.Model.Survey;
import TPRM.Model.SurveyTemplate;
import TPRM.Repository.SurveyRepository;
import TPRM.Repository.SurveyTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SurveyService {
    @Autowired
    private SurveyRepository surveyRepository;

    public List<Survey> findAllSurvey(){
        return (List<Survey>) surveyRepository.findAll();
    }
    public void save(Survey surveyTemplate){
        surveyRepository.save(surveyTemplate);
    }
    public Optional<Survey> findById(Long id){
        return surveyRepository.findById(id);
    }

}
