package TPRM.Config;

import TPRM.Service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;


//@Configuration
//@EnableAuthorizationServer
//public class OAuthConfiguration extends AuthorizationServerConfigurerAdapter {
//
//    private final PasswordEncoder passwordEncoder;
//
//    private final AuthenticationManager authenticationManager;
//
//    @Value("${jwt.clientId:glee-o-meter}")
//    private String clientId;
//
//    @Value("${jwt.client-secret:secret}")
//    private String clientSecret;
//
//    @Value("${jwt.signing-key:123}")
//    private String jwtSigningKey;
//
//    @Value("${jwt.authorizedGrantTypes:password,authorization_code,refresh_token}")
//    private String[] authorizedGrantTypes;
//
//    @Value("${jwt.accessTokenValidititySeconds:43200}") // 12 hours
//    private int accessTokenValiditySeconds;
//
//    @Value("${jwt.refreshTokenValiditySeconds:2592000}") // 30 days
//    private int refreshTokenValiditySeconds;
//
//    private final  UserDetailsService userDetailsService;
//
//    public OAuthConfiguration(AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, @Qualifier("detailsService") UserDetailsService userDetailsService){
//        this.userDetailsService = userDetailsService;
//        this.passwordEncoder = passwordEncoder;
//        this.authenticationManager = authenticationManager;
//    }
//
//    @Override
//    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
//        clients.inMemory()
//                .withClient(clientId)
//                .secret(passwordEncoder.encode(clientSecret))
//                .accessTokenValiditySeconds(accessTokenValiditySeconds)
//                .refreshTokenValiditySeconds(refreshTokenValiditySeconds)
//                .authorizedGrantTypes(authorizedGrantTypes)
//                .scopes("read", "write")
//                .resourceIds("api");
//    }
//
//    @Override
//    public void configure(final AuthorizationServerEndpointsConfigurer endpoints) {
//        endpoints
//                .accessTokenConverter(accessTokenConverter())
//                .userDetailsService(userDetailsService)
//                .authenticationManager(authenticationManager);
//    }
//
//    @Bean
//    JwtAccessTokenConverter accessTokenConverter() {
//        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//        return converter;
//    }
//
//}
