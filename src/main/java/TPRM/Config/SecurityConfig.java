package TPRM.Config;


//@EnableWebSecurity
//
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//    private BCryptPasswordEncoder bCryptPasswordEncoder;
//    private UserDetailsService userDetailsService;
//    private static final String[] AUTH_WHITELIST = {
//            "/v2/api-docs", "/swagger-resources", "/swagger-resources/**", "/configuration/ui", "/configuration/security", "/swagger-ui.html", "/webjars/**"
//        };
//
//    public SecurityConfig(@Qualifier("userService") UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder)
//        {
//        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
//        this.userDetailsService = userDetailsService;
//        }
//    protected void configure(HttpSecurity httpSecurity) throws Exception
//        {
//        httpSecurity.cors().and().csrf().disable().authorizeRequests()
//        .antMatchers(AUTH_WHITELIST).permitAll()
//        .antMatchers(HttpMethod.POST, "/cachedemo/v1/users/signup").permitAll()
//        .anyRequest().authenticated()
//        .and()
//                .addFilter(new AuthenticationFilter(authenticationManager()))
//        .addFilter(new AuthorizationFilter(authenticationManager()))
//        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//        }
//    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception
//        {
//        authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
//        }
//        @Bean
//        CorsConfigurationSource corsConfigurationSource()
//        {
//    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**",new CorsConfiguration().applyPermitDefaultValues());
//        return source;
//        }
//
//        @Bean
//        BCryptPasswordEncoder getbCryptPasswordEncoder(){
//            return new BCryptPasswordEncoder();
//        }
//}
