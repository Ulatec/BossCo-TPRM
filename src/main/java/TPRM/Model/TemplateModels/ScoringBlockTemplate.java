package TPRM.Model.TemplateModels;

import javax.persistence.*;
import java.util.List;
@Entity
public class ScoringBlockTemplate {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    @OneToMany
    private List<QuestionWeightPair> questionWeightPairs;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<QuestionWeightPair> getQuestionWeightpairs() {
        return questionWeightPairs;
    }

    public void setQuestions(List<QuestionWeightPair> questionWeightPairs) {
        this.questionWeightPairs = questionWeightPairs;
    }

    public List<QuestionWeightPair> getQuestionWeightPairs() {
        return questionWeightPairs;
    }

    public void setQuestionWeightPairs(List<QuestionWeightPair> questionWeightPairs) {
        this.questionWeightPairs = questionWeightPairs;
    }

    @Override
    public String toString() {
        return "ScoringBlockTemplate{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", questionWeightPairs=" + questionWeightPairs +
                '}';
    }
}
