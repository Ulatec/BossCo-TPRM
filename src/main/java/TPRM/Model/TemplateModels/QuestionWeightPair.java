package TPRM.Model.TemplateModels;

import TPRM.Model.Question.Question;

import javax.persistence.*;

@Entity
public class QuestionWeightPair {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Question question;
    private int weight;

    public QuestionWeightPair() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "QuestionWeightPair{" +
                "id=" + id +
                ", question=" + question +
                ", weight=" + weight +
                '}';
    }
}
