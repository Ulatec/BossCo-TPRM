package TPRM.Model.TemplateModels;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ScoreCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String categoryTitle;
    private double categoryWeight;
    public ScoreCategory(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public double getCategoryWeight() {
        return categoryWeight;
    }

    public void setCategoryWeight(double categoryWeight) {
        this.categoryWeight = categoryWeight;
    }
}
