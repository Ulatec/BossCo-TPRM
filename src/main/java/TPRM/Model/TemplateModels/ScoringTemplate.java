package TPRM.Model.TemplateModels;

import TPRM.Model.SurveyTemplate;

import javax.persistence.*;
import java.util.List;

@Entity
public class ScoringTemplate {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne
    private SurveyTemplate surveyTemplate;
    @OneToMany
    private List<ScoringBlockTemplate> scoringBlockTemplates;
    public ScoringTemplate(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SurveyTemplate getSurveyTemplate() {
        return surveyTemplate;
    }

    public void setSurveyTemplate(SurveyTemplate surveyTemplate) {
        this.surveyTemplate = surveyTemplate;
    }

    public List<ScoringBlockTemplate> getScoringBlockTemplates() {
        return scoringBlockTemplates;
    }

    public void setScoringBlockTemplates(List<ScoringBlockTemplate> scoringBlockTemplates) {
        this.scoringBlockTemplates = scoringBlockTemplates;
    }

    @Override
    public String toString() {
        return "ScoringTemplate{" +
                "id=" + id +
                ", surveyTemplate=" + surveyTemplate +
                ", scoringBlockTemplates=" + scoringBlockTemplates +
                '}';
    }
}
