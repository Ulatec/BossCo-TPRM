package TPRM.Model;

import TPRM.Model.Question.QuestionBlockWrapper;

import java.util.List;

public class SurveyQuestionBlockWrapper {
    private List<QuestionBlockWrapper> questionBlockWrapperList;
    private SurveyTemplate surveyTemplate;

    public List<QuestionBlockWrapper> getQuestionBlockWrapperList() {
        return questionBlockWrapperList;
    }

    public void setQuestionBlockWrapperList(List<QuestionBlockWrapper> questionBlockWrapperList) {
        this.questionBlockWrapperList = questionBlockWrapperList;
    }

    public SurveyTemplate getSurveyTemplate() {
        return surveyTemplate;
    }

    public void setSurveyTemplate(SurveyTemplate surveyTemplate) {
        this.surveyTemplate = surveyTemplate;
    }
}
