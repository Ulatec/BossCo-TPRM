package TPRM.Model.Wrappers;


import TPRM.Model.Question.Question;

public class BlockOrderWrapper {
    private Question question;
    private int location;

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }
}
