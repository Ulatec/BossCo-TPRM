package TPRM.Model.Wrappers;

import java.util.List;

public class BlockOrderContainer {
    private List<BlockOrderWrapper> blockOrderWrappers;

    public List<BlockOrderWrapper> getBlockOrderWrappers() {
        return blockOrderWrappers;
    }

    public void setBlockOrderWrappers(List<BlockOrderWrapper> blockOrderWrappers) {
        this.blockOrderWrappers = blockOrderWrappers;
    }
}
