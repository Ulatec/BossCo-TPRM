package TPRM.Model.Wrappers;

import TPRM.Model.SurveyTemplate;
import TPRM.Model.User;

import java.util.List;

public class AssignmentWrapper {
    private List<SurveyTemplate> surveyTemplates;
    private List<User> users;
    private SurveyTemplate surveyTemplate;
    private User user;

    public List<SurveyTemplate> getSurveyTemplates() {
        return surveyTemplates;
    }

    public void setSurveyTemplates(List<SurveyTemplate> surveyTemplates) {
        this.surveyTemplates = surveyTemplates;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public SurveyTemplate getSurveyTemplate() {
        return surveyTemplate;
    }

    public void setSurveyTemplate(SurveyTemplate surveyTemplate) {
        this.surveyTemplate = surveyTemplate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
