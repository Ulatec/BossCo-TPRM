package TPRM.Model;

import TPRM.Model.TemplateModels.ScoreCategory;

import javax.persistence.*;

@Entity
public class Score {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private ScoreCategory scoreCategory;
    private boolean score;
    public Score(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ScoreCategory getScoreCategory() {
        return scoreCategory;
    }

    public void setScoreCategory(ScoreCategory scoreCategory) {
        this.scoreCategory = scoreCategory;
    }

    public boolean isScore() {
        return score;
    }

    public void setScore(boolean score) {
        this.score = score;
    }
}
