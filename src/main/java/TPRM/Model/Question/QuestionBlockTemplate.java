package TPRM.Model.Question;

import TPRM.Model.Question.Question;
import TPRM.Model.Question.QuestionBlock;

import javax.persistence.*;
import java.util.List;

@Entity
public class QuestionBlockTemplate {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int revisions;
    private String title;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<QuestionBlock> questionBlocks;
    @OneToMany
    private List<Question> questions;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<QuestionBlock> getQuestionBlocks() {
        return questionBlocks;
    }

    public void setQuestionBlocks(List<QuestionBlock> questionBlocks) {
        this.questionBlocks = questionBlocks;
    }

    public int getRevisions() {
        return revisions;
    }

    public void setRevisions(int revisions) {
        this.revisions = revisions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "QuestionBlockTemplate{" +
                "id=" + id +
                ", revisions=" + revisions +
                ", title='" + title + '\'' +
                ", questionBlocks=" + questionBlocks +
                ", questions=" + questions +
                '}';
    }
}
