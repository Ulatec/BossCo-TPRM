package TPRM.Model.Question;

import TPRM.Model.Question.Question;
import TPRM.Static.QUESTIONTYPE;

import javax.persistence.*;

@Entity
public class QuestionAnswerPair {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String question;
    private String answer;
    @Column(columnDefinition = "boolean default false")
    private boolean notApplicable;
    private QUESTIONTYPE questionType;
    @ManyToOne
    private Question parentQuestion;
    @OneToOne
    private Question dependentQuestion;
//    @ManyToOne
//    private User user;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "QuestionAnswerPair{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", notApplicable=" + notApplicable +
                ", questionType=" + questionType +
                ", parentQuestion=" + parentQuestion +
                ", dependentQuestion=" + dependentQuestion +
                '}';
    }

    public boolean isNotApplicable() {
        return notApplicable;
    }

    public void setNotApplicable(boolean notApplicable) {
        this.notApplicable = notApplicable;
    }

    public QUESTIONTYPE getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QUESTIONTYPE questionType) {
        this.questionType = questionType;
    }

    public Question getDependentQuestion() {
        return dependentQuestion;
    }

    public void setDependentQuestion(Question dependentQuestion) {
        this.dependentQuestion = dependentQuestion;
    }

    public Question getParentQuestion() {
        return parentQuestion;
    }

    public void setParentQuestion(Question parentQuestion) {
        this.parentQuestion = parentQuestion;
    }

    //    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }
}
