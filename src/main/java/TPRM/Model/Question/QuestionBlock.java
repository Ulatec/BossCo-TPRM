package TPRM.Model.Question;

import javax.persistence.*;
import java.util.List;

@Entity
public class QuestionBlock {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<QuestionAnswerPair> questionAnswerPairs;
    @OneToOne
    private QuestionBlockTemplate questionBlockTemplate;
    private double version;
    private String title;

    public QuestionBlock(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<QuestionAnswerPair> getQuestionAnswerPairs() {
        return questionAnswerPairs;
    }

    public void setQuestionAnswerPairs(List<QuestionAnswerPair> questions) {
        this.questionAnswerPairs = questions;
    }

    public QuestionBlockTemplate getQuestionBlockTemplate() {
        return questionBlockTemplate;
    }

    public void setQuestionBlockTemplate(QuestionBlockTemplate questionBlockTemplate) {
        this.questionBlockTemplate = questionBlockTemplate;
    }

    public double getVersion() {
        return version;
    }

    public void setVersion(double version) {
        this.version = version;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "QuestionBlock{" +
                "id=" + id +
                ", questionAnswerPairs=" + questionAnswerPairs +
                ", questionBlockTemplate=" + questionBlockTemplate +
                ", version=" + version +
                ", title='" + title + '\'' +
                '}';
    }
}
