package TPRM.Model.Question;

import TPRM.Model.Answer;
import TPRM.Static.QUESTIONTYPE;

import javax.persistence.*;
import java.util.List;

@Entity
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String question;
    @OneToOne(cascade = CascadeType.PERSIST)
    private QuestionBlock questionBlock;
    private QUESTIONTYPE questionType;
    @Column(columnDefinition="BOOLEAN DEFAULT false")
    private boolean requiresDependentQuestion;
    @OneToOne
    private Question dependentQuestion;
    @OneToMany
    private List<Answer> answers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }


    public QuestionBlock getQuestionBlock() {
        return questionBlock;
    }

    public void setQuestionBlock(QuestionBlock questionBlock) {
        this.questionBlock = questionBlock;
    }

    public QUESTIONTYPE getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QUESTIONTYPE questionType) {
        this.questionType = questionType;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", questionBlock=" + questionBlock +
                ", questionType=" + questionType +
                ", requiresDependentQuestion=" + requiresDependentQuestion +
                ", dependentQuestion=" + dependentQuestion +
                ", answers=" + answers +
                '}';
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public Question getDependentQuestion() {
        return dependentQuestion;
    }

    public void setDependentQuestion(Question dependentQuestion) {
        this.dependentQuestion = dependentQuestion;
    }

    public boolean isRequiresDependentQuestion() {
        return requiresDependentQuestion;
    }

    public void setRequiresDependentQuestion(boolean requiresDependentQuestion) {
        this.requiresDependentQuestion = requiresDependentQuestion;
    }
}
