package TPRM.Model.Question;

import TPRM.Model.Answer;
import TPRM.Static.ACCOUNTTYPE;
import TPRM.Static.QUESTIONTYPE;

import java.util.List;

public class QuestionWrapper {

    private double tempId;
    private String question;
    private boolean requiresDependentQuestion;
    private double dependentId;
    private QUESTIONTYPE questionType;
    private List<Answer> answers;

    public QuestionWrapper(){

    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public double getTempId() {
        return tempId;
    }

    public void setTempId(double tempId) {
        this.tempId = tempId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isRequiresDependentQuestion() {
        return requiresDependentQuestion;
    }

    public void setRequiresDependentQuestion(boolean requiresDependentQuestion) {
        this.requiresDependentQuestion = requiresDependentQuestion;
    }

    public double getDependentId() {
        return dependentId;
    }

    public void setDependentId(double dependentId) {
        this.dependentId = dependentId;
    }

    public QUESTIONTYPE getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QUESTIONTYPE questionType) {
        this.questionType = questionType;
    }

    @Override
    public String toString() {
        return "QuestionWrapper{" +
                "tempId=" + tempId +
                ", question='" + question + '\'' +
                ", requiresDependentQuestion=" + requiresDependentQuestion +
                ", dependentId=" + dependentId +
                ", questionType=" + questionType +
                '}';
    }
}
