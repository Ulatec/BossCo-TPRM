package TPRM.Model.Question;

import java.util.List;


public class QuestionBlockCreationWrapper {

    private double version;
    private String title;
    List<QuestionWrapper> questionWrappers;
    public QuestionBlockCreationWrapper(){

    }

    public List<QuestionWrapper> getQuestionWrappers() {
        return questionWrappers;
    }

    public void setQuestionWrappers(List<QuestionWrapper> questionWrappers) {
        this.questionWrappers = questionWrappers;
    }

    public double getVersion() {
        return version;
    }

    public void setVersion(double version) {
        this.version = version;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "QuestionBlockCreationWrapper{" +
                "version=" + version +
                ", title='" + title + '\'' +
                ", questionWrappers=" + questionWrappers +
                '}';
    }
}
