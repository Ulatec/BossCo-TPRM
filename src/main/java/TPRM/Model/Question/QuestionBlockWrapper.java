package TPRM.Model.Question;

import TPRM.Model.Question.QuestionBlockTemplate;

public class QuestionBlockWrapper {
    private QuestionBlockTemplate questionBlockTemplate;
    private boolean enabled;

    public QuestionBlockTemplate getQuestionBlockTemplate() {
        return questionBlockTemplate;
    }

    public void setQuestionBlockTemplate(QuestionBlockTemplate questionBlockTemplate) {
        this.questionBlockTemplate = questionBlockTemplate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "QuestionBlockWrapper{" +
                "questionBlock=" + questionBlockTemplate +
                ", enabled=" + enabled +
                '}';
    }
}
