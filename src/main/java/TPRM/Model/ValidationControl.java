package TPRM.Model;

public class ValidationControl {
    private boolean increment;

    public boolean isIncrement() {
        return increment;
    }

    public void setIncrement(boolean increment) {
        this.increment = increment;
    }
}
