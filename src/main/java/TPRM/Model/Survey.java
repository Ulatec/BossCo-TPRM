package TPRM.Model;

import TPRM.Model.Question.QuestionBlock;

import javax.persistence.*;
import java.util.List;

@Entity
public class Survey {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @ManyToMany
    private List<QuestionBlock> questionBlockList;
    private String title;
    @ManyToOne
    private SurveyTemplate surveyTemplate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<QuestionBlock> getQuestionBlockList() {
        return questionBlockList;
    }

    public void setQuestionBlockList(List<QuestionBlock> questionBlockList) {
        this.questionBlockList = questionBlockList;
    }

    public SurveyTemplate getSurveyTemplate() {
        return surveyTemplate;
    }

    public void setSurveyTemplate(SurveyTemplate surveyTemplate) {
        this.surveyTemplate = surveyTemplate;
    }

    @Override
    public String toString() {
        return "Survey{" +
                "id=" + id +
                ", questionBlockList=" + questionBlockList +
                ", title='" + title + '\'' +
                ", surveyTemplate=" + surveyTemplate +
                '}';
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

