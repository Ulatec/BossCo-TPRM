package TPRM.Model;

import TPRM.Static.ACCOUNTTYPE;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
@Builder
@AllArgsConstructor
public class User implements UserDetails {

    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String username;
    @JsonIgnore
    private String password;
    private final Date creationDate;
    private Date lastModitifcation;
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Survey> surveys;
    private ACCOUNTTYPE accounttype;
    private Date lastLogin;
    private String email;
    public void setPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
    }

    public User(){
        creationDate = new Date();
    }
    public User(String username, String password, Set<Role> roles){
        creationDate = new Date();
        this.username = username;
        setPassword(password);
        this.roles = roles;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public String getPassword() {
        return password;
    }


    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Date getCreationDate() {
        return creationDate;
    }


    public Date getLastModitifcation() {
        return lastModitifcation;
    }

    public void setLastModitifcation(Date lastModitifcation) {
        this.lastModitifcation = lastModitifcation;
    }

    public List<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<Survey> surveys) {
        this.surveys = surveys;
    }

    public ACCOUNTTYPE getAccounttype() {
        return accounttype;
    }

    public void setAccounttype(ACCOUNTTYPE accounttype) {
        this.accounttype = accounttype;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", creationDate=" + creationDate +
                ", lastModitifcation=" + lastModitifcation +
                ", roles=" + roles +
                ", surveys=" + surveys +
                ", accounttype=" + accounttype +
                ", lastLogin=" + lastLogin +
                ", email='" + email + '\'' +
                '}';
    }
}
