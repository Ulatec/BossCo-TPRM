package TPRM.Model;

import TPRM.Model.Question.QuestionBlockTemplate;
import TPRM.Model.TemplateModels.ScoringTemplate;

import javax.persistence.*;
import java.util.List;

@Entity
public class SurveyTemplate {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<QuestionBlockTemplate> questionBlockTemplateList;
    private String title;
    @OneToOne
    private ScoringTemplate scoringTemplate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<QuestionBlockTemplate> getQuestionBlockTemplateList() {
        return questionBlockTemplateList;
    }

    public void setQuestionBlockTemplateList(List<QuestionBlockTemplate> questionBlockList) {
        this.questionBlockTemplateList = questionBlockList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "SurveyTemplate{" +
                "id=" + id +
                ", questionBlockList=" + questionBlockTemplateList +
                ", title='" + title + '\'' +
                '}';
    }

    public ScoringTemplate getScoringTemplate() {
        return scoringTemplate;
    }

    public void setScoringTemplate(ScoringTemplate scoringTemplate) {
        this.scoringTemplate = scoringTemplate;
    }
}
