package TPRM.Init;

import TPRM.Model.Question.Question;
import TPRM.Model.Question.QuestionBlockTemplate;
import TPRM.Model.Role;
import TPRM.Model.SurveyTemplate;
import TPRM.Model.User;
import TPRM.Repository.RoleRepository;
import TPRM.Service.BlockService;
import TPRM.Service.TemplateService;
import TPRM.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
@Order(0)
public class SampleDataInitializer implements ApplicationRunner {
    @Autowired
    private BlockService blockService;
    @Autowired
    private TemplateService templateService;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Optional<User> userOptional = userService.findByUsername("admin");
        if(userOptional.isEmpty()) {
            System.out.println("initializing sample data");
            // CREATE ADMIN ACCOUNT
            Role role = new Role();
            role.setRoleName("ADMIN");
            roleRepository.save(role);
            Set<Role> roles = new HashSet<>();
            roles.add(role);
            User user = new User();
            user.setUsername("admin");
            user.setRoles(roles);
            user.setPassword("admin");
            userService.save(user);
            //CREATE SURVEY
            SurveyTemplate surveyTemplate = new SurveyTemplate();
            surveyTemplate.setTitle("SIG Cybersecurity Survey");


            //CREATE QUESTION BLOCK #1
            QuestionBlockTemplate questionBlockTemplate = new QuestionBlockTemplate();
            questionBlockTemplate.setTitle("Risk Management");
            Question question = new Question();
            question.setQuestion("");
        }
    }
}
