package TPRM.Repository;

import TPRM.Model.Question.QuestionBlockTemplate;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface QuestionBlockTemplateRepository extends PagingAndSortingRepository<QuestionBlockTemplate, Long> {
    Optional<QuestionBlockTemplate> findByTitle(String title);
}
