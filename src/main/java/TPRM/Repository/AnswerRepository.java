package TPRM.Repository;

import TPRM.Model.Answer;
import TPRM.Model.Question.QuestionAnswerPair;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface AnswerRepository extends PagingAndSortingRepository<Answer, Long> {
}
