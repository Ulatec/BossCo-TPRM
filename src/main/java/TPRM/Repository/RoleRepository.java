package TPRM.Repository;

import TPRM.Model.Role;
import TPRM.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface RoleRepository extends JpaRepository<Role,Long> {
    Optional<Role> findByRoleName(String role);
}
