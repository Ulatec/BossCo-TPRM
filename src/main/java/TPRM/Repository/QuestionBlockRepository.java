package TPRM.Repository;

import TPRM.Model.Question.QuestionBlock;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface QuestionBlockRepository extends PagingAndSortingRepository<QuestionBlock, Long> {
}
