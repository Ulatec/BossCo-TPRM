package TPRM.Repository;

import TPRM.Model.Question.QuestionAnswerPair;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface QuestionAnswerPairRepository extends PagingAndSortingRepository<QuestionAnswerPair, Long> {
}
