package TPRM.Repository;

import TPRM.Model.Survey;
import TPRM.Model.SurveyTemplate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource
public interface SurveyTemplateRepository extends CrudRepository<SurveyTemplate, Long> {
}
