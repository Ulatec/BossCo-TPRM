package TPRM.Repository;

import TPRM.Model.SurveyTemplate;
import TPRM.Model.TemplateModels.ScoringTemplate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ScoringTemplateRepository extends CrudRepository<ScoringTemplate, Long> {

}
