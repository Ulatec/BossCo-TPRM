package TPRM.Repository;

import TPRM.Model.User;
import TPRM.Model.UserAction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UserActionRepository extends CrudRepository<UserAction,Long> {
}