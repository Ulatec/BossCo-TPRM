package TPRM.Controller.Web;

import TPRM.Controller.Web.Modal.Modal;
import TPRM.Model.*;
import TPRM.Model.Question.Question;
import TPRM.Model.Question.QuestionAnswerPair;
import TPRM.Model.Question.QuestionBlock;
import TPRM.Model.Question.QuestionBlockTemplate;
import TPRM.Model.Wrappers.AssignmentWrapper;
import TPRM.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class SurveyController {
    @Autowired
    private TemplateService templateService;
    @Autowired
    private UserService userService;
    @Autowired
    private SurveyService surveyService;
    @Autowired
    private BlockService blockService;
    @Autowired
    private QuestionAnswerPairService questionAnswerPairService;
    @Autowired
    private BlockTemplateService blockTemplateService;
    @RequestMapping(path = "/assignsurvey", method = RequestMethod.GET)
    private String viewtemplatestoassign(Model model){
        List<SurveyTemplate> templates = templateService.findAllTemplates();
        List<User> users = userService.findAllUsers();
        AssignmentWrapper assignmentWrapper = new AssignmentWrapper();
        assignmentWrapper.setSurveyTemplates(templates);
        assignmentWrapper.setUsers(users);
        System.out.println(assignmentWrapper.getSurveyTemplates());
        model.addAttribute("assignmentWrapper", assignmentWrapper);
        return "survey/assign_survey";
    }
    @RequestMapping(path = "/assignsurvey", method = RequestMethod.POST)
    private String assignSurvey(@ModelAttribute AssignmentWrapper assignmentWrapper, Model model){
        System.out.println("user: " + assignmentWrapper.getUser());
        System.out.println("surveyTemplate: " + assignmentWrapper.getSurveyTemplate());


        System.out.println(assignmentWrapper.getSurveyTemplates());
        createNewSurvey(assignmentWrapper);
        model.addAttribute("assignmentWrapper", assignmentWrapper);
        return "survey/assign_survey";
    }
    @RequestMapping(path = "/admin/view_survey_list", method = RequestMethod.GET)
    private String viewSurveyList(Model model){
        List<Survey> surveys = surveyService.findAllSurvey();
        model.addAttribute("surveys", surveys);
        return "admin/survey/view_survey_list";
    }

    @RequestMapping(path = "/edit_survey/{id}", method = RequestMethod.GET)
    private String edit_survey(@PathVariable Long id, Model model){
        Optional<Survey> survey = surveyService.findById(id);

        if(survey.isPresent()) {
            model.addAttribute("survey", survey.get());
            model.addAttribute("action", "/edit_survey/post/" + id);
        }
        return "admin/survey/edit_survey";
    }
    @RequestMapping(path = "/edit_survey/post/{id}", method = RequestMethod.POST)
    private String edit_survey(@ModelAttribute Survey survey, Model model){
        Survey oldSurvey = surveyService.findById(survey.getId()).get();
        oldSurvey.setTitle(survey.getTitle());
        surveyService.save(oldSurvey);
        return "admin/survey/edit_survey";
    }
    @RequestMapping(path = "/fill_survey/{id}", method = RequestMethod.GET)
    private String fillSurvey(@PathVariable Long id, Model model){
        Survey oldSurvey = surveyService.findById(id).get();
        User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        System.out.println("Pulled Survey: " + oldSurvey);
        if(user.getSurveys().contains(oldSurvey)){
            model.addAttribute("survey", oldSurvey);
            model.addAttribute("action", "/fill_survey/post/" + id);
        }else{
            Modal modal = new Modal("You do not have permission to access this survey.", Modal.Status.FAILURE);
            model.addAttribute("modal", modal);
            System.out.println("not able to access this survey.");
            System.out.println("ENDING.");
            return "survey/available_survey";
        }
        return "survey/fill_survey";
    }
    @RequestMapping(path = "/fill_survey/post/{id}", method = RequestMethod.POST)
    private String postSurvey(@ModelAttribute Survey survey, Model model){
        Survey oldSurvey = surveyService.findById(survey.getId()).get();
        System.out.println("NEW SURVEY: " + survey);
        User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        System.out.println("Pulled Survey: " + oldSurvey);
        for(QuestionBlock questionBlock : survey.getQuestionBlockList()){
            for(QuestionAnswerPair questionAnswerPair : questionBlock.getQuestionAnswerPairs()){
                System.out.println(questionAnswerPair);
                QuestionAnswerPair oldQuestionAnswerPair = questionAnswerPairService.findById(questionAnswerPair.getId()).get();
                oldQuestionAnswerPair.setAnswer(questionAnswerPair.getAnswer());
                oldQuestionAnswerPair.setNotApplicable(questionAnswerPair.isNotApplicable());
                questionAnswerPairService.save(oldQuestionAnswerPair);
            }
        }
        if(user.getSurveys().contains(oldSurvey)){
            model.addAttribute("survey", oldSurvey);
            model.addAttribute("action", "/fill_survey/post/" + survey.getId());
        }else{
            System.out.println("not able to access this survey.");
            System.out.println("ENDING.");
        }
        return "survey/fill_survey";
    }

    public boolean createNewSurvey(AssignmentWrapper assignmentWrapper){
        User targetUser = assignmentWrapper.getUser();
        //Check if user already has a survey of template type
        List<Survey> surveyList = targetUser.getSurveys();
        System.out.println(surveyList);
        boolean surveyFound = false;
        for(Survey survey : surveyList){
            if(survey.getSurveyTemplate().getId().equals(assignmentWrapper.getSurveyTemplate().getId())){
                System.out.println("User already has a survey from this template.");
                surveyFound = true;
            }
        }
        if(!surveyFound){
            Survey survey = new Survey();
            survey.setSurveyTemplate(assignmentWrapper.getSurveyTemplate());
            Long id = assignmentWrapper.getSurveyTemplate().getId();
            SurveyTemplate existingTemplate = templateService.findById(id).get();

            System.out.println("CHOSEN SURVEY TEMPLATE =" + existingTemplate);
            System.out.println("QUESTIONBLOCKLIST =" + existingTemplate.getQuestionBlockTemplateList());
            List<QuestionBlock> questionBlocks = new ArrayList<>();
            int i = 0;
            for(QuestionBlockTemplate questionBlock : existingTemplate.getQuestionBlockTemplateList()){
                System.out.println(questionBlock.getTitle());
                QuestionBlock questionBlock1 = new QuestionBlock();
                List<QuestionAnswerPair> questionAnswerPairList = new ArrayList<>();
                for(Question question : questionBlock.getQuestions()){
                    System.out.println(question.getQuestion());
                    QuestionAnswerPair questionAnswerPair = new QuestionAnswerPair();
                    questionAnswerPair.setQuestion(question.getQuestion());
                    questionAnswerPair.setQuestionType(question.getQuestionType());
                    questionAnswerPair.setDependentQuestion(question.getDependentQuestion());
                    questionAnswerPair.setParentQuestion(question);
//                    questionAnswerPair.setUser(targetUser);
                    questionAnswerPairList.add(questionAnswerPair);
                    questionAnswerPairService.save(questionAnswerPair);
                    i++;
                    System.out.println(i);
                }
                questionBlock1.setQuestionAnswerPairs(questionAnswerPairList);
                questionBlock1.setTitle(questionBlock.getTitle());
                blockService.save(questionBlock1);
                questionBlocks.add(questionBlock1);
            }
            survey.setQuestionBlockList(questionBlocks);
            survey.setTitle(assignmentWrapper.getSurveyTemplate().getTitle());
            System.out.println("new survey: " + survey);
            surveyService.save(survey);
            targetUser.getSurveys().add(survey);
            userService.save(targetUser);
            System.out.println("Survey successfully assigned to " + targetUser);
        }else{
            System.out.println("Unable to assign new survey");
        }

        return false;
    }
}
