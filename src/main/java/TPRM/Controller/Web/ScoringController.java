package TPRM.Controller.Web;

import TPRM.Model.Question.Question;
import TPRM.Model.Question.QuestionBlockTemplate;
import TPRM.Model.SurveyTemplate;
import TPRM.Model.TemplateModels.QuestionWeightPair;
import TPRM.Model.TemplateModels.ScoringBlockTemplate;
import TPRM.Model.TemplateModels.ScoringTemplate;
import TPRM.Service.ScoringTemplateService;
import TPRM.Service.SurveyService;
import TPRM.Service.TemplateService;
import TPRM.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class ScoringController {
    @Autowired
    private TemplateService templateService;
    @Autowired
    private UserService userService;
    @Autowired
    private SurveyService surveyService;
    @Autowired
    private ScoringTemplateService scoringTemplateService;

    @RequestMapping(path = "/createScoringTemplate", method = RequestMethod.GET)
    public String listSurveyScoreList(Model model){
        List<SurveyTemplate> surveyTemplateList = templateService.findAllTemplates();
        model.addAttribute("surveyTemplateList", surveyTemplateList);
        return "scoring/create_scoring_template";
    }
    @RequestMapping(path = "/editScoringTemplate/{id}", method = RequestMethod.GET)
    public String editScoringTemplate(@PathVariable Long id, Model model){
        Optional<SurveyTemplate> surveyTemplate = templateService.findById(id);
        if(surveyTemplate.isPresent()){
            model.addAttribute("surveyTemplate", surveyTemplate.get());
            if(surveyTemplate.get().getScoringTemplate() != null){
                ScoringTemplate scoringTemplate = buildScoringTemplate(surveyTemplate.get());
                model.addAttribute("scoringTemplate", scoringTemplate);
                System.out.println(scoringTemplate);
            }else{
                ScoringTemplate scoringTemplate = buildScoringTemplate(surveyTemplate.get());
                model.addAttribute("scoringTemplate", scoringTemplate);
                System.out.println(scoringTemplate);
//                model.addAttribute("scoringTemplate", new ScoringTemplate());
            }
            return "scoring/edit_scoring_template";
        }else{
            return "scoring/create_scoring_template";
        }
    }
    @RequestMapping(path = "/editScoringTemplate/{id}", method = RequestMethod.POST)
    public String postScoringTemplate(@ModelAttribute ScoringTemplate scoringTemplate, Model model){
        System.out.println(scoringTemplate);
        Long id = scoringTemplate.getSurveyTemplate().getId();
        Optional<SurveyTemplate> surveyTemplateOptional = templateService.findById(id);
        if(surveyTemplateOptional.isPresent()){
            SurveyTemplate surveyTemplate = surveyTemplateOptional.get();
            surveyTemplate.setScoringTemplate(scoringTemplate);
            scoringTemplateService.save(scoringTemplate);
            templateService.save(surveyTemplate);
        }
//        Optional<SurveyTemplate> surveyTemplate = templateService.findById(id);
//        if(surveyTemplate.isPresent()){
//            model.addAttribute("surveyTemplate", surveyTemplate.get());
//            if(surveyTemplate.get().getScoringTemplate() != null){
//                ScoringTemplate scoringTemplate = buildScoringTemplate(surveyTemplate.get());
//                model.addAttribute("scoringTemplate", scoringTemplate);
//                System.out.println(scoringTemplate);
//            }else{
//                ScoringTemplate scoringTemplate = buildScoringTemplate(surveyTemplate.get());
//                model.addAttribute("scoringTemplate", scoringTemplate);
////                model.addAttribute("scoringTemplate", new ScoringTemplate());
//            }
//            return "scoring/edit_scoring_template";
//        }else{
            return "redirect:createScoringTemplate";
        //}
    }

    private ScoringTemplate buildScoringTemplate(SurveyTemplate surveyTemplate){
        ScoringTemplate scoringTemplate = new ScoringTemplate();
        List<ScoringBlockTemplate> scoringBlockTemplates = new ArrayList<>();
        for(QuestionBlockTemplate questionBlockTemplate : surveyTemplate.getQuestionBlockTemplateList()){
            ScoringBlockTemplate scoringBlockTemplate = new ScoringBlockTemplate();
            //scoringBlockTemplate.setQuestionBlocks(questionBlockTemplate.getQuestionBlocks());
            scoringBlockTemplate.setTitle(questionBlockTemplate.getTitle());
            scoringBlockTemplate.setQuestions(new ArrayList<>());
            for(Question question : questionBlockTemplate.getQuestions()){
                QuestionWeightPair questionWeightPair = new QuestionWeightPair();
                questionWeightPair.setQuestion(question);
                questionWeightPair.setWeight(0);
                scoringBlockTemplate.getQuestionWeightpairs().add(questionWeightPair);
            }
            scoringBlockTemplates.add(scoringBlockTemplate);
        }
        scoringTemplate.setSurveyTemplate(surveyTemplate);
        scoringTemplate.setScoringBlockTemplates(scoringBlockTemplates);
        return scoringTemplate;
    }
}
