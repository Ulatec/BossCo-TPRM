package TPRM.Controller.Web;

import TPRM.Model.User;
import TPRM.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class MainWebController {
    @Autowired
    private TemplateService templateService;
    @Autowired
    private UserService userService;
    @Autowired
    private SurveyService surveyService;
    @Autowired
    private BlockService blockService;
    @Autowired
    private QuestionAnswerPairService questionAnswerPairService;
    @Autowired
    private BlockTemplateService blockTemplateService;
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String login(Model model, HttpServletRequest request){
        model.addAttribute("user", new User());
        List<String> roleStrings = new ArrayList<>();
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        //String[] userRoles = userService.findByUsername(username).get().getRoles();
        //roleStrings.addAll(Arrays.asList(userRoles));
        System.out.println(userService.findByUsername(username).get());
        if(roleStrings.contains("ROLE_ADMIN")){
            return "admin_index";
        }else{
            return "user_index";
        }

    }
}
