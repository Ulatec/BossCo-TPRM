package TPRM.Controller.Web;

import TPRM.Controller.Web.Modal.Modal;
import TPRM.Model.Survey;
import TPRM.Model.User;
import TPRM.Service.SurveyService;
import TPRM.Service.TemplateService;
import TPRM.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {
    @Autowired
    private TemplateService templateService;
    @Autowired
    private UserService userService;
    @Autowired
    private SurveyService surveyService;
    @RequestMapping(path = "/profile/", method = RequestMethod.GET)
    public String getProfile(Model model){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.findByUsername(username).get();
        model.addAttribute("user",user);
        return "/user/user_profile";
    }
    @RequestMapping(path = "/user/edit_user_info", method = RequestMethod.GET)
    public String editProfile(Model model){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.findByUsername(username).get();
        model.addAttribute("user",user);
        return "/user/edit_user_information";
    }
    @RequestMapping(path = "/user/edit_user_info", method = RequestMethod.POST)
    public String postProfile(@ModelAttribute User user, Model model){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User userToBind = userService.findByUsername(username).get();
        System.out.println("Email: " + user.getEmail());
        if(checkEmailFormat(user.getEmail())){
            userToBind.setEmail(user.getEmail());
            userService.save(userToBind);
            Modal modal = new Modal("Information successfully updated.", Modal.Status.SUCCESS);
            model.addAttribute("modal", modal);
        }else{
            Modal modal = new Modal("Email is invalid. Try Again.", Modal.Status.FAILURE);
            model.addAttribute("modal", modal);
        }
        return "/user/edit_user_information";

    }



    private boolean checkEmailFormat(String email){
        if(!email.contains("@")){
            return false;
        }
        if(!email.contains(".")){
            return false;
        }
        return email.indexOf("@") < email.indexOf(".");
    }
}
