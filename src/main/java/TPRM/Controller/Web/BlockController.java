package TPRM.Controller.Web;

import TPRM.Controller.Web.Utils.BlockUtils;
import TPRM.Controller.Web.Validation.BlockValidationController;
import TPRM.Model.*;
import TPRM.Model.Question.Question;
import TPRM.Model.Question.QuestionBlockCreationWrapper;
import TPRM.Model.Question.QuestionBlockTemplate;
import TPRM.Model.Question.QuestionWrapper;
import TPRM.Model.Wrappers.BlockOrderContainer;
import TPRM.Model.Wrappers.BlockOrderWrapper;
import TPRM.Service.AnswerService;
import TPRM.Service.BlockTemplateService;
import TPRM.Service.QuestionService;
import TPRM.Service.TemplateService;
import TPRM.Static.QUESTIONTYPE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.*;

@Controller
public class BlockController {
    @Autowired
    private TemplateService templateService;
    @Autowired
    private BlockTemplateService blockTemplateService;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private AnswerService answerService;

    @RequestMapping(path = "/blocks", method = RequestMethod.GET)
    private String viewAllBlocks(Model model){
        List<QuestionBlockTemplate> questionBlocks = blockTemplateService.findAllBlocks();
        System.out.println(questionBlocks.size());
        for(QuestionBlockTemplate questionBlock : questionBlocks){
            System.out.println(questionBlock.getTitle());
        }
        model.addAttribute("questionBlocks", questionBlocks);
        return "viewblocks";
    }
    @RequestMapping(path = "/createblock", method = RequestMethod.GET)
    private String createBlock(Model model){
        QuestionBlockCreationWrapper questionBlockCreationWrapper = new QuestionBlockCreationWrapper();
//        QuestionBlock questionBlock = new QuestionBlock();
        List<Enum> enumValues = Arrays.asList(QUESTIONTYPE.values());
        model.addAttribute("questionBlockCreationWrapper", questionBlockCreationWrapper);
        model.addAttribute("types", enumValues);
//        model.addAttribute("questionBlock", questionBlock);
        model.addAttribute("action", "/postblock");
        return "block/create_block";
    }

    @RequestMapping(path = "/viewBlockAnswers", method = RequestMethod.GET)
    private String viewBlockAnswers(Model model){

        HashMap<QuestionBlockTemplate, Boolean> hasAnswersMap = new HashMap<>();
        List<QuestionBlockTemplate> questionBlockTemplates = blockTemplateService.findAllBlocks();
        for(QuestionBlockTemplate questionBlockTemplate : questionBlockTemplates) {
            boolean flag = true;
            for (Question question : questionBlockTemplate.getQuestions()) {
                if (question.getAnswers() == null) {
                    flag = false;
                    break;
                } else if (question.getAnswers().size() == 0) {
                    flag = false;
                    break;
                }
            }

            hasAnswersMap.put(questionBlockTemplate, flag);
        }
//        QuestionBlock questionBlock = new QuestionBlock();
        List<Enum> enumValues = Arrays.asList(QUESTIONTYPE.values());
        model.addAttribute("hasAnswersMap", hasAnswersMap);
        model.addAttribute("types", enumValues);
        return "block/block_answers";
    }

    @RequestMapping(path = "/block/edit_block_answers/{id}", method = RequestMethod.GET)
    private String createBlockAnswers(@PathVariable Long id, Model model){
        QuestionBlockTemplate questionBlockTemplate = blockTemplateService.findById(id).get();
        System.out.println(questionBlockTemplate);
//        QuestionBlock questionBlock = new QuestionBlock();
        List<Enum> enumValues = Arrays.asList(QUESTIONTYPE.values());
        model.addAttribute("questionBlockTemplate", questionBlockTemplate);
        model.addAttribute("action", "/block/post_block_answers/" + id);
        model.addAttribute("types", enumValues);
        return "block/edit_block_answers";
    }
    @RequestMapping(path = "/block/post_block_answers/{id}", method = RequestMethod.POST)
    private String postBlockAnswers(@Valid QuestionBlockTemplate questionBlock, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        System.out.println("post");
        System.out.println(questionBlock);
        System.out.println(bindingResult);
        for(Question question : questionBlock.getQuestions()){
            Long id = question.getId();
            Question savedQuestion = questionService.findById(id).get();
            for(Answer answer : question.getAnswers()){
                if(answer.getId() == null){
                    answerService.save(answer);
                }
            }
            System.out.println(question);



            savedQuestion.setAnswers(question.getAnswers());
            questionService.save(savedQuestion);
        }
        return "redirect:/viewBlockAnswers";
    }
    @RequestMapping(path = "/block/editblock/{id}", method = RequestMethod.GET)
    private String editBlock(@PathVariable Long id, Model model){
        System.out.println("edit block #" + id);
        QuestionBlockTemplate questionBlock = blockTemplateService.findById(id).get();
        ValidationControl validationControl = new ValidationControl();
        validationControl.setIncrement(true);
        model.addAttribute("questionBlock", questionBlock);
        model.addAttribute("action", "/block/editblock/" + id);
        return "block/create_block";
    }

    @RequestMapping(path = "/block/editblock/edit_order/{id}", method = RequestMethod.GET)
    private String editQuestionBlockOrder(@PathVariable Long id, Model model){
        System.out.println("edit block #" + id);
        QuestionBlockTemplate questionBlock = blockTemplateService.findById(id).get();
        ValidationControl validationControl = new ValidationControl();
        validationControl.setIncrement(true);
        int i =0;
        List<BlockOrderWrapper> blockOrderWrappers = new ArrayList<>();
        for(Question question : questionBlock.getQuestions()){
            BlockOrderWrapper blockOrderWrapper = new BlockOrderWrapper();
            blockOrderWrapper.setQuestion(question);
            blockOrderWrapper.setLocation(i);
            blockOrderWrappers.add(blockOrderWrapper);
            i++;
        }
        BlockOrderContainer blockOrderContainer = new BlockOrderContainer();
        blockOrderContainer.setBlockOrderWrappers(blockOrderWrappers);
        model.addAttribute("blockOrderContainer", blockOrderContainer);
        model.addAttribute("action", "/block/editblock/edit_order/" + id);
        return "block/edit_block_question_order";
    }
    @RequestMapping(path = "/block/editblock/edit_order/{id}", method = RequestMethod.POST)
    private String submitQuestionBlockOrder(@PathVariable Long id, @ModelAttribute BlockOrderContainer blockOrderContainer, Model model){
        System.out.println("edit block #" + id);
        QuestionBlockTemplate questionBlock = blockTemplateService.findById(id).get();
//        ValidationControl validationControl = new ValidationControl();
//        validationControl.setIncrement(true);

        boolean allPresent = BlockUtils.allNumbersPresent(blockOrderContainer);
        if(allPresent){
            boolean anyMissing = BlockUtils.anyDuplicates(blockOrderContainer);
            if(anyMissing){
                System.out.println("ERROR DUPLICATES");
            }else{
                System.out.println("NO DUPLICATES, AND ALL PRESENT.");
                List<BlockOrderWrapper> blockOrderWrappers = new ArrayList<>();
                int pos = 0;
                for(BlockOrderWrapper blockOrderWrapper : blockOrderContainer.getBlockOrderWrappers()){
                    if(blockOrderWrapper.getLocation() == pos){
                        blockOrderWrappers.add(blockOrderWrapper);
                    }
                    pos++;
                }
                List<Question> questions = new ArrayList<>();
                for(BlockOrderWrapper wrapper : blockOrderWrappers){
                    questions.add(wrapper.getQuestion());
                }
                questionBlock.setQuestions(questions);
                blockTemplateService.save(questionBlock);
            }
        }else{
            System.out.println("NOT ALL NUMEBRS PRESENT");
        }
        int i =0;
        List<BlockOrderWrapper> blockOrderWrappers = new ArrayList<>();
        for(Question question : questionBlock.getQuestions()){
            BlockOrderWrapper blockOrderWrapper = new BlockOrderWrapper();
            blockOrderWrapper.setQuestion(question);
            blockOrderWrapper.setLocation(i);
            blockOrderWrappers.add(blockOrderWrapper);
            i++;
        }
        BlockOrderContainer blockOrderContainer1 = new BlockOrderContainer();
        blockOrderContainer1.setBlockOrderWrappers(blockOrderWrappers);
        model.addAttribute("blockOrderContainer", blockOrderContainer1);
        model.addAttribute("action", "/block/editblock/edit_order/" + id);
        return "block/edit_block_question_order";
    }


    @RequestMapping(path = "/block/viewBlock/{id}", method = RequestMethod.GET)
    private String viewBlock(@PathVariable Long id, Model model){
        System.out.println("edit block #" + id);
        QuestionBlockTemplate questionBlock = blockTemplateService.findById(id).get();
        ValidationControl validationControl = new ValidationControl();
        validationControl.setIncrement(true);
        model.addAttribute("validationControl", validationControl);
        model.addAttribute("questionBlock", questionBlock);
        model.addAttribute("action", "/block/editblock/" + id);
        return "view_block";
    }


    @RequestMapping(path = "/block/editblock/{id}", method = RequestMethod.POST)
    private String postBlock(@Valid QuestionBlockTemplate questionBlock, BindingResult bindingResult, RedirectAttributes redirectAttributes){

        System.out.println("block received");
        System.out.println(bindingResult.getModel());
        if(questionBlock.getTitle().replace(" ", "").equals("")){
            redirectAttributes.addFlashAttribute("Invalid title.", FlashMessage.Status.FAILURE);
            System.out.println("Rejecting block. Invalid title.");
            return String.format("redirect:/blocks" , questionBlock.getId());
        }
        if(bindingResult.hasErrors()){
            for(FieldError error : bindingResult.getFieldErrors()){
                System.out.println(error.toString());
            }
            return String.format("redirect:/blocks" , questionBlock.getId());
        }
        Optional<QuestionBlockTemplate> existingQuestionBlock = blockTemplateService.findById(questionBlock.getId());
        if(existingQuestionBlock.isPresent()){
            System.out.println("Let's apply form values.");
            blockTemplateService.save(applyFormValues(questionBlock, existingQuestionBlock.get()));
        }else{
            Iterator<Question> questionIterator = questionBlock.getQuestions().iterator();
            List<Question> cleanQuestions = new ArrayList<>();
            while(questionIterator.hasNext()){
                Question question = questionIterator.next();
                if(!question.getQuestion().equals("")) {
                    System.out.println("saving question " + question.getQuestion());
                    //question.setQuestionBlock(questionBlock);
                    questionService.save(question);
                    cleanQuestions.add(question);
                }
            }
            questionBlock.setQuestions(cleanQuestions);
            blockTemplateService.save(questionBlock);
            return String.format("redirect:/blocks" , questionBlock.getId());
        }

        return "viewblocks";
    }

    @RequestMapping(path = "/block/deletequestion/{id}", method = RequestMethod.POST)
    private String deleteQuestion(@PathVariable Long id,  RedirectAttributes redirectAttributes){

        System.out.println("Delete block");
        Optional<Question> question = questionService.findById(id);
        if(question.isPresent()){
            System.out.println("Question found. ");
            System.out.println("ID: " + question.get().getId());
            System.out.println("Question: " + question.get().getQuestion());
            Optional<QuestionBlockTemplate> questionBlockOptional = blockTemplateService.findById(question.get().getQuestionBlock().getId());
            if(questionBlockOptional.isPresent()){
                System.out.println("Name of linked Block: " + questionBlockOptional.get().getTitle());
                QuestionBlockTemplate questionBlock = questionBlockOptional.get();
                List<Question> questions = questionBlock.getQuestions();
                questions.removeIf(n -> n.getId().equals(id));
                questionBlock.setQuestions(questions);
                blockTemplateService.save(questionBlock);
                questionService.delete(question.get());
            }
        }

        return "viewblocks";
    }



    @RequestMapping(path = "/postblock", method = RequestMethod.POST)
    public String submitNewQuestionBlock(@Valid QuestionBlockCreationWrapper questionBlockCreationWrapper,
                                         BindingResult bindingResult, RedirectAttributes redirectAttributes){
//        BlockValidationController blockValidationController = new BlockValidationController(blockTemplateService);
//        if(!blockValidationController.isNewBlockSafeToCommit(questionBlockCreationWrapper)){
//            System.out.println("Invalid block.");
//            return "redirect:/createblock";
//        }


        System.out.println(questionBlockCreationWrapper);
        QuestionBlockTemplate questionBlockTemplate = new QuestionBlockTemplate();
        questionBlockTemplate.setTitle(questionBlockCreationWrapper.getTitle());
        questionBlockTemplate.setQuestions(new ArrayList<>());

        HashMap<Double, Question> completedQuestions = new HashMap<>();
        for(QuestionWrapper questionWrapper : questionBlockCreationWrapper.getQuestionWrappers()){
            Question question = new Question();
            question.setQuestionType(questionWrapper.getQuestionType());
            question.setQuestion(questionWrapper.getQuestion());
            if(questionWrapper.isRequiresDependentQuestion()){
                double tempId = questionWrapper.getDependentId();
                Question parentQuestion = completedQuestions.get(tempId);
                question.setDependentQuestion(parentQuestion);
                question.setRequiresDependentQuestion(true);
            }
            completedQuestions.put(questionWrapper.getTempId(), question);
            questionBlockTemplate.getQuestions().add(question);
        }

        List<Question> cleanQuestions = new ArrayList<>();
        for (Question question : questionBlockTemplate.getQuestions()) {
            if (!question.getQuestion().equals("")) {
                System.out.println("saving question " + question.getQuestion());
                //question.setQuestionBlock(questionBlock);
                questionService.save(question);
                cleanQuestions.add(question);
            }
        }


        questionBlockTemplate.setQuestions(cleanQuestions);


        for(Question question : cleanQuestions) {
            questionService.save(question);
        }
        blockTemplateService.save(questionBlockTemplate);



        System.out.println("READBACK:");

        return "redirect:/";
    }

    public QuestionBlockTemplate applyFormValues(QuestionBlockTemplate newQuestionBlockFromForm, QuestionBlockTemplate existingQuestionBlock){

        //Save Ingredients
        Iterator<Question> questionIterator = newQuestionBlockFromForm.getQuestions().iterator();
        List<Question> cleanQuestions = new ArrayList<>();
        while(questionIterator.hasNext()){
            Question question = questionIterator.next();
            if(!question.getQuestion().equals("")) {
                System.out.println("saving question " + question.getQuestion());
                //question.setQuestionBlock(existingQuestionBlock);
                questionService.save(question);
                cleanQuestions.add(question);
            }
        }


        existingQuestionBlock.setQuestions(cleanQuestions);

        return existingQuestionBlock;
    }
}
