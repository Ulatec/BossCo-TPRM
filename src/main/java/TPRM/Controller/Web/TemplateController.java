package TPRM.Controller.Web;

import TPRM.Model.*;
import TPRM.Model.Question.QuestionBlockTemplate;
import TPRM.Model.Question.QuestionBlockWrapper;
import TPRM.Model.Wrappers.SurveyQuestionBlockWrapper;
import TPRM.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class TemplateController {
    @Autowired
    private TemplateService templateService;
    @Autowired
    private BlockTemplateService blockTemplateService;
    @Autowired
    private BlockService blockService;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private UserService userService;
    @Autowired
    private SurveyService surveyService;
    @RequestMapping(path = "/templates", method = RequestMethod.GET)
    private String viewAllTemplates(Model model){
        List<SurveyTemplate> templates = templateService.findAllTemplates();
        model.addAttribute("templates", templates);
        return "survey/templates";
    }

    @RequestMapping(path = "/user/surveys", method = RequestMethod.GET)
    private String viewUserSurveys(Model model){
        User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        System.out.println("SURVEYS:");
        System.out.println(user.getSurveys());

        model.addAttribute("surveys", user.getSurveys());
        return "survey/available_survey";
    }

    @RequestMapping(path = "/createtemplate", method = RequestMethod.GET)
    private String createTemplate(Model model){
        List<QuestionBlockTemplate> questionBlocks = blockTemplateService.findAllBlocks();
        SurveyQuestionBlockWrapper surveyQuestionBlockWrapper = new SurveyQuestionBlockWrapper();
        List<QuestionBlockWrapper> wrapperList = new ArrayList<>();
        for(QuestionBlockTemplate questionBlock : questionBlocks){
            QuestionBlockWrapper questionBlockWrapper = new QuestionBlockWrapper();
            questionBlockWrapper.setQuestionBlockTemplate(questionBlock);
            questionBlockWrapper.setEnabled(false);
            wrapperList.add(questionBlockWrapper);
        }
        surveyQuestionBlockWrapper.setQuestionBlockWrapperList(wrapperList);

        for(QuestionBlockWrapper questionBlockWrapper : surveyQuestionBlockWrapper.getQuestionBlockWrapperList()){
            System.out.println(questionBlockWrapper.getQuestionBlockTemplate().getTitle() + " : " + questionBlockWrapper.getQuestionBlockTemplate().getId());
        }
        surveyQuestionBlockWrapper.setSurveyTemplate(new SurveyTemplate());
        System.out.println(surveyQuestionBlockWrapper);
        model.addAttribute("surveyQuestionBlockWrapper", surveyQuestionBlockWrapper);
        model.addAttribute("action", "/query/submitQuery");
        return "survey/create_template";
    }
    @RequestMapping(value = "/templates/viewtemplate/{id}", method = RequestMethod.GET)
    public String viewTemplate(@PathVariable Long id, Model model) {
        Optional<SurveyTemplate> surveyTemplate = templateService.findById(id);
        if(surveyTemplate.isPresent()){
            System.out.println(surveyTemplate);
            model.addAttribute("surveyTemplate", surveyTemplate.get());
        }else{
            System.out.println("template not found.");
        }

        return "survey/view_template";
    }

    public String editTemplate(@PathVariable Long id, Model model){
        Optional<SurveyTemplate> surveyTemplate = templateService.findById(id);

        if(surveyTemplate.isPresent()) {
            model.addAttribute("surveyTemplate", surveyTemplate.get());
        }else{
            System.out.println("couldn't find template.");
            return "redirect:/templates";
        }
        return "survey/edit_template";
    }



    @RequestMapping(value = "/query/submitQuery", method = RequestMethod.POST)
    public String processQuery(@ModelAttribute SurveyQuestionBlockWrapper surveyQuestionBlockWrapper, Model model, RedirectAttributes redirectAttributes) {
        if(surveyQuestionBlockWrapper.getSurveyTemplate().getTitle().replace(" ", "").equals("")){
            redirectAttributes.addFlashAttribute("Invalid title.", FlashMessage.Status.FAILURE);
            System.out.println("Rejecting block. Invalid title.");
            return String.format("redirect:/createtemplate");
        }
        System.out.println(surveyQuestionBlockWrapper.getQuestionBlockWrapperList());
        System.out.println(surveyQuestionBlockWrapper.getQuestionBlockWrapperList().size());
        List<QuestionBlockWrapper> list = surveyQuestionBlockWrapper.getQuestionBlockWrapperList();
        System.out.println(list);
        for(QuestionBlockWrapper questionBlockWrapper : surveyQuestionBlockWrapper.getQuestionBlockWrapperList()){
            System.out.println(questionBlockWrapper);
            System.out.println(questionBlockWrapper.getQuestionBlockTemplate().getId() + " : " + questionBlockWrapper.isEnabled());
        }
        List<QuestionBlockTemplate> enabledBlocks = returnEnabledQuestionBlocks(surveyQuestionBlockWrapper.getQuestionBlockWrapperList());

        System.out.println(surveyQuestionBlockWrapper.getQuestionBlockWrapperList() != null ? surveyQuestionBlockWrapper.getQuestionBlockWrapperList().size() : "null list");
        System.out.println("--");
        SurveyTemplate surveyTemplate = new SurveyTemplate();
        surveyTemplate.setTitle(surveyQuestionBlockWrapper.getSurveyTemplate().getTitle());
        System.out.println("saving " + enabledBlocks.size() + " questions");

        surveyTemplate.setQuestionBlockTemplateList(enabledBlocks);
        templateService.save(surveyTemplate);
        model.addAttribute("surveyQuestionBlockWrapper", surveyQuestionBlockWrapper);
        model.addAttribute("action", "/query/submitQuery");
        return "survey/create_template";
    }


    private List<QuestionBlockTemplate> returnEnabledQuestionBlocks(List<QuestionBlockWrapper> questionBlockWrapperList){
        List<QuestionBlockTemplate> enabledBlocks = new ArrayList<>();
        for(QuestionBlockWrapper questionBlockWrapper : questionBlockWrapperList){
            if(questionBlockWrapper.isEnabled()) {
                Optional<QuestionBlockTemplate> questionBlock = blockTemplateService.findById(questionBlockWrapper.getQuestionBlockTemplate().getId());
                if (questionBlock.isPresent()) {
                    System.out.println("questionBlock found");
                    enabledBlocks.add(questionBlock.get());
                } else {
                    System.out.println("questionBlock Not found.");
                }
            }
        }
        return  enabledBlocks;
    }

//
//    public QuestionBlock applyFormValues(QuestionBlock newQuestionBlockFromForm, QuestionBlock existingQuestionBlock){
//
//        //Save Ingredients
//        newQuestionBlockFromForm.getQuestions().forEach(
//                Question -> questionService.save(Question));
//
//        return existingQuestionBlock;
//    }
}
