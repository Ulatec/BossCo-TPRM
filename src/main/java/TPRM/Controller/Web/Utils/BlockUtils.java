package TPRM.Controller.Web.Utils;

import TPRM.Model.Wrappers.BlockOrderContainer;
import TPRM.Model.Wrappers.BlockOrderWrapper;

import java.util.ArrayList;
import java.util.List;

public class BlockUtils {
    public static boolean allNumbersPresent(BlockOrderContainer blockOrderContainer){
        int size = blockOrderContainer.getBlockOrderWrappers().size();
        System.out.println("SIZE: " + size);
        for(int i = 0; i<size; i++){
            boolean found = false;
            for(BlockOrderWrapper blockOrderWrapper : blockOrderContainer.getBlockOrderWrappers()){
                System.out.println("location: " +blockOrderWrapper.getLocation());
                if(blockOrderWrapper.getLocation() == i){
                    System.out.println("FOUND");
                    found = true;
                    break;
                }
            }
            if(!found){
                return false;
            }
        }
        return true;
    }
    public static boolean anyDuplicates(BlockOrderContainer blockOrderContainer){
        List<Integer> integers = new ArrayList<>();
        for(BlockOrderWrapper blockOrderWrapper : blockOrderContainer.getBlockOrderWrappers()){
            if(integers.contains(blockOrderWrapper.getLocation())){
                return true;
            }else{
                integers.add(blockOrderWrapper.getLocation());
            }
        }
        return false;
    }
}
