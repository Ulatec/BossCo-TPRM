package TPRM.Controller.Web;

import TPRM.Controller.Web.Modal.Modal;
import TPRM.Model.User;
import TPRM.Service.UserService;
import TPRM.Static.ACCOUNTTYPE;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class LoginController {
    @Autowired
    private UserService users;

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String login(Model model, HttpServletRequest request){
        model.addAttribute("user", new User());
        return "login";
    }
    @RequestMapping(path = "/user_index", method = RequestMethod.GET)
    public String user_login(Model model, HttpServletRequest request){
        System.out.println("Successful Login.");
        Modal modal = new Modal("test", Modal.Status.SUCCESS);
        model.addAttribute("modal", modal);
        //model.addAttribute("user", new User());
        return "user_index";
    }
//    public User getAuthenticatedUser() {
//        if(SecurityContextHolder.getContext().getAuthentication() != null){
//            String username = SecurityContextHolder.getContext().getAuthentication().getName();
//            return users.findByUsername(username).get();
//        } else{
//            throw new AccessDeniedException("Please sign in.");
//        }
//    }
//    @ExceptionHandler(value = AccessDeniedException.class)
//    public String accessDeniedHandler(HttpServletRequest request, AccessDeniedException ex) {
//        return "redirect:/login";
//    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signupSubmit(@Valid User user, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        System.out.println(user);
        if (bindingResult.hasErrors()) {
            return "redirect:/signup";
        }
        if (users.findByUsername(user.getUsername()) != null) {
            //redirectAttributes.addFlashAttribute("flash", new FlashMessage("Username is already taken", FlashMessage.Status.FAILURE));
            return "redirect:/signup";
        } else {
            //user.setRoles(new String[]{"ROLE_" + user.getAccounttype()});
            users.save(user);
            return "redirect:/login";
        }
    }
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginSubmit(@Valid User user, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        System.out.println("Login initiated");
        List<String> roleStrings = new ArrayList<>();
        //String[] userRoles = users.findByUsername(user.getUsername()).get().getRoles();
//
        return null;
    }
    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(Model model) {
        List<Enum> enumValues = Arrays.asList(ACCOUNTTYPE.values());
        model.addAttribute("enumValues", enumValues);
        System.out.println(enumValues);
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", new User());
        }
        return "signup";
    }
}
