package TPRM.Controller.Web.Validation;

import TPRM.Model.Question.Question;
import TPRM.Model.Question.QuestionBlockCreationWrapper;
import TPRM.Model.Question.QuestionBlockTemplate;
import TPRM.Model.Question.QuestionWrapper;
import TPRM.Service.BlockService;
import TPRM.Service.BlockTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Block;
import org.springframework.stereotype.Controller;

import java.util.Optional;


public class BlockValidationController {

    private BlockTemplateService blockTemplateService;

    public BlockValidationController(BlockTemplateService blockTemplateService){
        this.blockTemplateService = blockTemplateService;
    }

    public boolean isNewBlockSafeToCommit(QuestionBlockCreationWrapper questionBlockCreationWrapper){
        if(!isTitleValid(questionBlockCreationWrapper)) {
            return false;
        }
//        if(areQuestionsBlank(questionBlockCreationWrapper)){
//            return false;
//        }
        if(doesTitleAlreadyExist(questionBlockCreationWrapper)){
            return false;
        }
        return true;
    }


    public boolean isTitleValid(QuestionBlockCreationWrapper questionBlockCreationWrapper){
        return !(questionBlockCreationWrapper.getTitle().replace(" ", "").equals(""));
    }
    public boolean areQuestionsBlank(QuestionBlockCreationWrapper questionBlockCreationWrapper){
        for(QuestionWrapper question : questionBlockCreationWrapper.getQuestionWrappers()){
            if(question.getQuestion().replace(" ", "").equals("")){
                return true;
            }
        }
        return false;
    }
    public boolean doesTitleAlreadyExist(QuestionBlockCreationWrapper questionBlockCreationWrapper){
        Optional<QuestionBlockTemplate> questionBlockTemplateOptional = blockTemplateService.findByName(questionBlockCreationWrapper.getTitle());
        return questionBlockTemplateOptional.isPresent();
    }

}
