package TPRM.Controller.Rest;

import TPRM.Model.Question.QuestionBlockTemplate;
import TPRM.Model.User;
import TPRM.Service.BlockTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/blockTemplate")
public class BlockTemplateRestController {

    @Autowired
    private BlockTemplateService blockTemplateService;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<QuestionBlockTemplate> getAllQuestionBlocks(){
        return blockTemplateService.findAllBlocks();
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public QuestionBlockTemplate saveNewBlockTemplate(@Validated @RequestBody QuestionBlockTemplate questionBlockTemplate){
        System.out.println("Submitted Question Block Template: " + questionBlockTemplate);
        if(!blockTemplateService.questionBlockTitleIsBlank(questionBlockTemplate)){
            blockTemplateService.save(questionBlockTemplate);
            return questionBlockTemplate;
        }else{
            return new QuestionBlockTemplate();
        }
    }
}
