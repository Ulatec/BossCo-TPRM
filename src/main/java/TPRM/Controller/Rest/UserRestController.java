package TPRM.Controller.Rest;

import TPRM.Controller.Rest.Exception.ResourceNotFoundException;

import TPRM.Controller.Rest.Exception.NotAuthorizedException;
import TPRM.Model.User;
import TPRM.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserRestController {
    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/all")
    public List<User> findAll(){
        return userService.findAllUsers();
    }

    @PreAuthorize("authenticated")
    @GetMapping("/{id}")
    public User findUserById(@PathVariable("id") Long id){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<User> userOptional = userService.findByUsername(username);
        System.out.println(userOptional);
        if(userOptional.isPresent()){
            if(userOptional.get().getId().equals(id)){
                return userOptional.get();
            }else{
                //CHECK IF ADMIN
                if(userOptional.get().getRoles().stream().anyMatch(p -> p.getRoleName().equals("ADMIN"))){
                    return userOptional.get();
                }
                throw new NotAuthorizedException("Access Forbidden");
            }
        }
        Optional<User> optionalUser = userService.findById(id);
        if(optionalUser.isPresent()){
            return optionalUser.get();
        }else{
            throw new ResourceNotFoundException("User does not exist.");
        }
    }

    @PostMapping
    public User saveUser(@Validated @RequestBody User user) {
        System.out.println(user);
        if(userService.doesUsernameExist(user)){
            userService.save(user);
            return user;
        }else{
            return new User();
        }
    }


}
