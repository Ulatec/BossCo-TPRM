package TPRM.Controller.Rest;

import TPRM.Model.AuthBody;
import TPRM.Model.Role;
import TPRM.Model.User;
import TPRM.Repository.RoleRepository;
import TPRM.Repository.UserRepository;
import TPRM.Service.UserService;
import TPRM.jwt.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    UserRepository users;

    @Autowired
    private UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @SuppressWarnings("rawtypes")
    @PostMapping("/login")
    public ResponseEntity login(@RequestBody AuthBody data) {
        System.out.println("login attempt");
        System.out.println(data);
        try {
            String username = data.getUsername();
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(username, data.getPassword());
            authenticationManager.authenticate(usernamePasswordAuthenticationToken);
            String token = jwtTokenProvider.createToken(username, this.users.findByUsername(username).get().getRoles());
            Map<Object, Object> model = new HashMap<>();
            model.put("username", username);
            model.put("token", token);
            return ok(model);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("err");
            throw new BadCredentialsException("Invalid email/password supplied");
        }
    }

    @SuppressWarnings("rawtypes")
    @PostMapping("/register")
    public ResponseEntity register(@RequestBody AuthBody data) {
        System.out.println(data);
        System.out.println("register attempt");
        List<User> userList = userService.findAllByUsername(data.getUsername());
        if(userList.size()>0){
            throw new BadCredentialsException("User with username: " + data.getUsername() + " already exists");
        }
        Optional<Role> roleOptional = roleRepository.findByRoleName("USER");
        if(!roleOptional.isPresent()) {
            Role role = new Role();
            role.setRoleName("USER");
            roleRepository.save(role);
        }
        User user = new User();
        user.setUsername(data.getUsername());
        user.setPassword(data.getPassword());
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(roleRepository.findByRoleName("USER").get());
        user.setRoles(roleSet);
        userService.save(user);

        Optional<User> userExists1 = userService.findByUsername(data.getUsername());
        if(userExists1.isPresent()){
            System.out.println("Verification that User is in DB.");
        }else{
            System.out.println("User not in DB.");
        }
        Map<Object, Object> model = new HashMap<>();
        model.put("message", "User registered successfully");
        return ok(model);
    }
}